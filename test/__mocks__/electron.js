import fs from 'fs';
import path from 'path';

const settingsPath =
  '/Users/rherrick/Development/XNAT/Tools/Clients/Scaffold/xnat-scaffold';
fs.unlinkSync(path.resolve(settingsPath, 'Settings'));
module.exports = {
  app: {
    getPath: () => settingsPath
  }
};

import Registry from '../../app/plugins/Registry';

const registry = new Registry();
console.log(registry.plugins);
describe('Registry', () => {
  it('has no plugins to start', () => {
    console.error(`X ${registry.plugins} w/ ${registry.plugins.length} items`);
    expect(registry.plugins).toEqual(expect.arrayContaining([]));
    const serialized = registry.plugins.map(
      plugin =>
        `${plugin.id}:${plugin.version}:${plugin.location}:${plugin.enabled}`
    );
    expect(serialized).toEqual(expect.arrayContaining([]));
    const plugin = registry.createPlugin('id', '1.0', 'over there', true);
    expect(registry.plugins).toEqual(expect.arrayContaining([plugin]));
    expect(plugin).toHaveProperty('id', 'id');
    expect(plugin).toHaveProperty('version', '1.0');
    expect(plugin).toHaveProperty('location', 'over there');
    expect(plugin).toHaveProperty('enabled', true);
  });
});

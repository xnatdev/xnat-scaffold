// @flow
import React, { Component } from 'react';
import XnatInventory from '../components/XnatInventory';

type Props = {};

export default class XnatInventoryPage extends Component<Props> {
  props: Props;

  render() {
    return <XnatInventory />;
  }
}

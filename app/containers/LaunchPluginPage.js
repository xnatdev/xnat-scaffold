/* eslint-disable react/prop-types */
// @flow
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import LaunchPlugin from '../components/LaunchPlugin';
import ScaffoldPlugin from '../plugins/ScaffoldPlugin';
import Registry from '../plugins/Registry';
import ErrorBoundary from '../components/ErrorBoundary';

type Props = {
  match: any
};

class LaunchPluginPage extends Component<Props> {
  props: Props;

  plugin: ScaffoldPlugin;

  constructor(props) {
    super(props);

    const { pluginId } = props.match.params;
    this.plugin = new Registry().findPlugin(pluginId);
    const { servers } = props.location.state;
    this.servers = servers;
  }

  render() {
    return (
      <ErrorBoundary>
        <LaunchPlugin plugin={this.plugin} servers={this.servers} />
      </ErrorBoundary>
    );
  }
}

export default withRouter(LaunchPluginPage);

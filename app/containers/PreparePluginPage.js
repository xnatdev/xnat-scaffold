// @flow
import React, { Component } from 'react';
import PreparePlugin from '../components/PreparePlugin';

type Props = {};

export default class PreparePluginPage extends Component<Props> {
  props: Props;

  render() {
    return <PreparePlugin />;
  }
}

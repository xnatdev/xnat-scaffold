// @flow
import React, { Component } from 'react';
import ManageServer from '../components/ManageServer';

type Props = {};

export default class ManageServerPage extends Component<Props> {
  props: Props;

  render() {
    return <ManageServer />;
  }
}

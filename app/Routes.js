import React from 'react';
import { Switch, Route } from 'react-router';
import routes from './constants/routes';
import App from './containers/App';
import XnatInventoryPage from './containers/XnatInventoryPage';
import ManageServerPage from './containers/ManageServerPage';
import LaunchPluginPage from './containers/LaunchPluginPage';
import LazyLoadModule from './plugins/LazyLoadModule';
import Registry from './plugins/Registry';
import ManagePluginPage from './containers/ManagePluginPage';
import PreparePluginPage from './containers/PreparePluginPage';

const registry = new Registry();

const pluginRoutes = registry.plugins.map(plugin => (
  <Route
    path={plugin.key}
    component={props => (
      <LazyLoadModule {...props} resolve={() => import(plugin.value)} />
    )}
  />
));

const RouteWithSubRoutes = route => (
  <Route
    path={route.path}
    render={props => (
      // pass the sub-routes down to keep nesting
      <route.component {...props} routes={route.routes} />
    )}
  />
);

export default () => (
  <App>
    <Switch>
      <Route path={routes.LAUNCH_PLUGIN} component={LaunchPluginPage} />
      <Route path={routes.MANAGE_PLUGIN} component={ManagePluginPage} />
      <Route path={routes.MANAGE_SERVER} component={ManageServerPage} />
      <Route path={routes.PREPARE_PLUGIN} component={PreparePluginPage} />
      <Route path={routes.SERVERS} component={XnatInventoryPage} />
      {pluginRoutes.map(route => (
        <RouteWithSubRoutes key={route.path} {...route} />
      ))}
    </Switch>
  </App>
);

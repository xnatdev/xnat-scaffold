// @flow
import React, { Component } from 'react';
import Modal from 'react-modal';
import { Link } from 'react-router-dom';
import OverlayTrigger from 'react-bootstrap/es/OverlayTrigger';
import Tooltip from 'react-bootstrap/es/Tooltip';
import Button from 'react-bootstrap/es/Button';
import ButtonGroup from 'react-bootstrap/es/ButtonGroup';
import ScaffoldPlugin from '../plugins/ScaffoldPlugin';
import Registry from '../plugins/Registry';

Modal.setAppElement('body');

type Props = {
  registry: Registry,
  plugin: ScaffoldPlugin,
  openModal: (
    modalTitle: string,
    modalMessage: string,
    onOk: () => void,
    onCancel: () => void
  ) => void
};

export default class XnatPluginPanel extends Component<Props> {
  props: Props;

  constructor(props) {
    super(props);

    this.registry = props.registry;
    this.plugin = props.plugin;
    this.openModal = props.openModal;

    this.promptToDelete = this.promptToDelete.bind(this);
  }

  promptToDelete() {
    console.log(`Delete plugin ${this.plugin.display}?`);
    this.openModal(
      'Are you sure?',
      `You're about to delete the plugin "${
        this.plugin.display
      }". Click 'OK' to continue or 'Cancel' to return.`,
      () => {
        console.log(`Deleting plugin ${this.plugin.display}`);
        this.registry.removePlugin(this.plugin.id);
      }
    );
  }

  render() {
    return (
      <div className="inventory-list-item">
        <OverlayTrigger
          key={`trigger-plugin-${this.plugin.id}`}
          placement="bottom"
          overlay={
            <Tooltip
              id={`tooltip-plugin-${this.plugin.id}`}
              className="user-name text-left small"
            >
              Plugin: {this.plugin.display}
            </Tooltip>
          }
        >
          <ButtonGroup id={`plugin-${this.plugin.id}`}>
            <Link to={`/plugins/${this.plugin.id}/prepare`}>
              <Button
                variant="outline-primary btn-bigfriendly"
                data-key={this.plugin.id}
                block="true"
              >
                <b>{this.plugin.display}</b>
              </Button>
            </Link>
            <Link to={`/managePlugin/${this.plugin.id}`}>
              <Button
                variant="outline-primary btn-bigfriendly"
                data-key={`edit-${this.plugin.id}`}
              >
                <img src="../resources/icons/file-edit-16.png" alt="Edit" />
              </Button>
            </Link>
            <Button
              variant="outline-primary btn-bigfriendly"
              data-key={`delete-${this.plugin.id}`}
              onClick={() => this.promptToDelete(this.plugin.id)}
            >
              <img src="../resources/icons/file-delete-16.png" alt="Delete" />
            </Button>
          </ButtonGroup>
        </OverlayTrigger>
      </div>
    );
  }
}

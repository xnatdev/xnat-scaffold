// @flow
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Modal from 'react-modal';
import OverlayTrigger from 'react-bootstrap/es/OverlayTrigger';
import Button from 'react-bootstrap/es/Button';
import Tabs from 'react-bootstrap/es/Tabs';
import Tab from 'react-bootstrap/es/Tab';
import Tooltip from 'react-bootstrap/es/Tooltip';
import ServerStore from '@xnat/xnat-js/src/auth/ServerStore';
import routes from '../constants/routes';
import XnatPluginList from './XnatPluginList';
import XnatServerList from './XnatServerList';
import Registry from '../plugins/Registry';

type Props = {};

export default class XnatInventory extends Component<Props> {
  props: Props;

  constructor(props) {
    super(props);

    this.servers = new ServerStore();
    this.registry = new Registry();

    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.onOk = this.onOk.bind(this);
    this.onCancel = this.onCancel.bind(this);
    this.closeModal = this.closeModal.bind(this);

    this.state = {
      modalIsOpen: false,
      modalTitle: '',
      modalMessage: ''
    };
  }

  openModal(modalTitle, modalMessage, onOk: null, onCancel: null) {
    this.setState({
      modalIsOpen: true,
      modalTitle,
      modalMessage,
      onOk: onOk || this.closeModal,
      onCancel
    });
  }

  afterOpenModal() {
    const { modalTitle } = this.state;
    console.log(`Opened modal with title ${modalTitle}`);
  }

  onOk() {
    const { onOk } = this.state;
    if (onOk) {
      onOk();
    }
    this.closeModal();
  }

  onCancel() {
    const { onCancel } = this.state;
    if (onCancel) {
      onCancel();
    }
    this.closeModal();
  }

  closeModal() {
    this.setState({ modalIsOpen: false, modalTitle: '', modalMessage: '' });
  }

  render() {
    const { modalIsOpen, modalTitle, modalMessage } = this.state;
    return (
      <div className="container" data-tid="container">
        <div className="container-fluid">
          <Modal
            isOpen={modalIsOpen}
            onRequestClose={this.closeModal}
            contentLabel="authResult"
          >
            <h3>{modalTitle}</h3>
            <div>{modalMessage}</div>
            <button type="button" onClick={this.onOk}>
              OK
            </button>
            <button type="button" onClick={this.onCancel}>
              Cancel
            </button>
          </Modal>
          <div className="row">
            <div className="logo col-md-5">
              <img src="../resources/images/xnat_logo.jpg" alt="" />
              <div className="intro-text noselect">
                <p>
                  Welcome to XNAT Scaffold, where lonely medical apps can find a
                  friendly backend to consume and interact with.
                </p>
              </div>
            </div>
            <div className="col-md-7">
              <div className="row">
                <div className="col-12 text-left">
                  <Tabs defaultActiveKey="servers" id="main-tabs">
                    <Tab eventKey="servers" title="Servers">
                      <Link to={routes.MANAGE_SERVER}>
                        <OverlayTrigger
                          key="trigger-add-new-xnat"
                          placement="bottom"
                          overlay={
                            <Tooltip id="id-add-new-xnat">
                              Add new XNAT server
                            </Tooltip>
                          }
                        >
                          <Button
                            variant="outline-primary btn-bigfriendly"
                            data-toggle="modal"
                            data-target="#login"
                          >
                            <i className="fa fa-plus" />
                            <i className="fa fa-database" />
                            Add new XNAT server
                          </Button>
                        </OverlayTrigger>
                      </Link>
                      <XnatServerList
                        servers={this.servers}
                        openModal={this.openModal}
                        afterOpenModal={this.afterOpenModal}
                        closeModal={this.closeModal}
                      />
                    </Tab>
                    <Tab eventKey="plugins" title="Plugins">
                      <Link to={routes.MANAGE_PLUGIN}>
                        <OverlayTrigger
                          key="trigger-add-new-plugin"
                          placement="bottom"
                          overlay={
                            <Tooltip id="id-add-new-plugin">
                              Add new XNAT plugin
                            </Tooltip>
                          }
                        >
                          <Button
                            variant="outline-primary btn-bigfriendly"
                            data-toggle="modal"
                            data-target="#login"
                          >
                            <i className="fa fa-plus" />
                            <i className="fa fa-database" />
                            Add new XNAT plugin
                          </Button>
                        </OverlayTrigger>
                      </Link>
                      <XnatPluginList
                        registry={this.registry}
                        openModal={this.openModal}
                        afterOpenModal={this.afterOpenModal}
                        closeModal={this.closeModal}
                      />
                    </Tab>
                  </Tabs>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

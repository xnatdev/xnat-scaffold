/* eslint-disable flowtype/no-weak-types */
// @flow
import React, { Component } from 'react';
import Modal from 'react-modal';
import { Link, withRouter } from 'react-router-dom';
import ServerStore from '@xnat/xnat-js/src/auth/ServerStore';
import routes from '../constants/routes';

type Props = {
  match: any
};

Modal.setAppElement('body');

class ManageServer extends Component<Props> {
  props: Props;

  constructor(props) {
    super(props);

    this.servers = new ServerStore();

    const { serverKey } = props.match.params;

    let state = {};
    if (serverKey && serverKey !== ':serverKey') {
      const server = this.servers.getServer(serverKey);
      this.pageTitle = `Edit ${server.address} for user ${server.username}`;
      state = {
        key: server.key,
        address: server.address,
        alias: server.alias,
        username: server.username,
        password: server.password,
        allowInsecureSsl: server.allowInsecureSsl
      };
    } else {
      this.pageTitle = 'Add XNAT Server';
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.onFieldChange = this.onFieldChange.bind(this);
    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);

    this.state = {
      modalIsOpen: false,
      modalTitle: '',
      modalMessage: '',
      ...state
    };
  }

  handleSubmit(evt) {
    console.log(this.state);

    evt.preventDefault();

    const { address, allowInsecureSsl, alias, username, password } = this.state;
    // eslint-disable-next-line react/prop-types
    const { history } = this.props;

    const server = this.servers.saveServerProperties(
      address,
      alias || `${username}@${address}`,
      username,
      password,
      allowInsecureSsl || false
    );
    this.servers
      .loginPromise(server)
      .then(response => {
        console.log(response);
        if (response.status < 400) {
          history.push(routes.SERVERS);
          return true;
        }
        this.openModal(
          'Authentication error',
          `Something went wrong while trying to authenticate on the server "${
            server.address
          }" as the user "${server.username}": Got response ${response.status}`
        );
        return false;
      })
      .catch(error =>
        this.openModal(
          'Authentication error',
          `An error occurred while trying to authenticate on the server "${
            server.address
          }" as the user "${server.username}": ${error}`
        )
      );
  }

  onFieldChange(property, evt) {
    const state = {};

    state[property] =
      evt.target.type === 'checkbox' ? evt.target.checked : evt.target.value;

    // console.log(state);

    this.setState(state);
  }

  openModal(modalTitle, modalMessage) {
    this.setState({ modalIsOpen: true, modalTitle, modalMessage });
  }

  afterOpenModal() {
    const { modalTitle } = this.state;
    console.log(`Opened modal with title ${modalTitle}`);
  }

  closeModal() {
    this.setState({ modalIsOpen: false, modalTitle: '', modalMessage: '' });
  }

  render() {
    const {
      modalIsOpen,
      modalTitle,
      modalMessage,
      address,
      alias,
      username,
      password,
      allowInsecureSsl
    } = this.state;
    return (
      <div id="manageServer" className="container-fluid">
        <Modal
          isOpen={modalIsOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          contentLabel="authResult"
        >
          <h3>{modalTitle}</h3>
          <div>{modalMessage}</div>
          <button type="button" onClick={this.closeModal}>
            Close
          </button>
        </Modal>
        <div className="modal-content">
          <form id="manageServerForm" onSubmit={this.handleSubmit}>
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLongTitle">
                {this.pageTitle}
              </h5>
            </div>
            <div className="modal-body">
              <div className="form-group">
                <label htmlFor="server" className="col-form-label">
                  Server:
                </label>
                <input
                  type="text"
                  className="form-control"
                  name="address"
                  id="address"
                  placeholder="xnat.server.url"
                  defaultValue={address}
                  required
                  onChange={evt => {
                    this.onFieldChange('address', evt);
                  }}
                />
              </div>
              <div className="form-check text-right">
                <input
                  className="form-check-input"
                  type="checkbox"
                  defaultChecked={allowInsecureSsl}
                  name="allow_insecure_ssl"
                  id="allow_insecure_ssl"
                  onChange={evt => {
                    this.onFieldChange('allowInsecureSsl', evt);
                  }}
                />
                <label
                  className="form-check-label"
                  htmlFor="allow_insecure_ssl"
                >
                  Allow unverified certificates
                </label>
              </div>
              <div className="form-group">
                <label htmlFor="server" className="col-form-label">
                  Alias:
                </label>
                <input
                  type="text"
                  className="form-control"
                  name="alias"
                  id="alias"
                  defaultValue={alias}
                  placeholder="(Optional) Give your XNAT an alias name."
                  required
                  onChange={evt => {
                    this.onFieldChange('alias', evt);
                  }}
                />
              </div>
              <div className="form-group">
                <label htmlFor="username" className="col-form-label">
                  Username:
                </label>
                <input
                  type="text"
                  className="form-control"
                  name="username"
                  id="username"
                  defaultValue={username}
                  onChange={evt => {
                    this.onFieldChange('username', evt);
                  }}
                />
              </div>
              <div className="form-group">
                <label htmlFor="password" className="col-form-label">
                  Password:
                </label>
                <input
                  type="password"
                  className="form-control"
                  name="password"
                  id="password"
                  defaultValue={password}
                  onChange={evt => {
                    this.onFieldChange('password', evt);
                  }}
                />
              </div>
            </div>
            <div className="modal-footer">
              <Link to={routes.SERVERS}>
                <button
                  type="button"
                  className="btn btn-gray"
                  data-dismiss="modal"
                >
                  Cancel
                </button>
              </Link>
              <button type="submit" className="btn btn-blue">
                Login
              </button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default withRouter(ManageServer);

// @flow
import React, { Component } from 'react';
import '../app.global.css';
import Registry from '../plugins/Registry';
import XnatPluginPanel from './XnatPluginPanel';

type Props = {
  registry: Registry,
  openModal: (
    modalTitle: string,
    modalMessage: string,
    onOk: () => void,
    onCancel: () => void
  ) => void
};

export default class XnatPluginList extends Component<Props> {
  props: Props;

  constructor(props) {
    super(props);

    this.registry = props.registry;
  }

  render() {
    const { openModal } = this.props;
    const pluginList = this.registry.plugins.map(plugin => (
      <XnatPluginPanel
        key={plugin.id}
        plugin={plugin}
        openModal={openModal}
        registry={this.registry}
      />
    ));
    return <div className="">{pluginList}</div>;
  }
}

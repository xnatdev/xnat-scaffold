/* eslint-disable flowtype/no-weak-types */
// @flow
import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import Server from '@xnat/xnat-js/src/auth/Server';
import Button from 'react-bootstrap/es/Button';
import ScaffoldPlugin from '../plugins/ScaffoldPlugin';
// import LazyLoadModule from '../plugins/LazyLoadModule';
import routes from '../constants/routes';
import { whatIsIt, check } from '../plugins/PluginModule';

type Props = {
  plugin: ScaffoldPlugin,
  servers: Server[]
};

class LaunchPlugin extends Component<Props> {
  plugin: ScaffoldPlugin;

  servers: Server[];

  constructor(props) {
    super(props);

    this.plugin = props.plugin;
    this.servers = props.servers || [];
  }

  render() {
    // const PluginInstance = React.lazy(() =>
    //   import(path.relative('.', this.plugin.location))
    // );
    // const PluginInstance = React.lazy(() =>
    //   import('/Users/rherrick/Development/XNAT/Tools/Clients/Scaffold/foo-plugin/app/containers/HelloWorldPage')
    // );
    // <LazyLoadModule resolve={() => import(this.plugin.location)} servers={this.servers} />
    // <Suspense fallback={<div>Loading plugin ${this.plugin.name}...</div>}>
    //   <PluginInstance servers={this.servers} />
    // </Suspense>
    const what = whatIsIt();
    const checked = check('foo');
    console.log(`* what: ${typeof what}\n* checked: ${typeof checked}`);

    return (
      <div>
        <dl>
          <dd>What is it?</dd>
          <dt>{what}</dt>
          <dd>Check</dd>
          <dt>{checked}</dt>
        </dl>
        <Link to={routes.SERVERS}>
          <Button variant="outline-primary btn-bigfriendly">Go back</Button>
        </Link>
      </div>
    );
  }
}

export default withRouter(LaunchPlugin);

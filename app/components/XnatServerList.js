// @flow
import React, { Component } from 'react';
import ServerStore from '@xnat/xnat-js/src/auth/ServerStore';
import XnatServerPanel from './XnatServerPanel';
import '../app.global.css';

type Props = {
  servers: ServerStore,
  openModal: (
    modalTitle: string,
    modalMessage: string,
    onOk: () => void,
    onCancel: () => void
  ) => void
};

export default class XnatServerList extends Component<Props> {
  props: Props;

  constructor(props) {
    super(props);

    this.state = {
      servers: props.servers
    };
  }

  render() {
    console.log(this.state);

    const { servers } = this.state;
    const { openModal } = this.props;
    const serverList = servers
      .getServers()
      .map(server => (
        <XnatServerPanel
          key={server.key}
          server={server}
          servers={servers}
          openModal={openModal}
        />
      ));
    return <div className="">{serverList}</div>;
  }
}

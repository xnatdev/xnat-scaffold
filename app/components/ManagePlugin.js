// @flow
import React, { Component } from 'react';
import Modal from 'react-modal';
import { Link, withRouter } from 'react-router-dom';
import fs from 'fs';
import path from 'path';
import unzipper from 'unzipper';
import Button from 'react-bootstrap/es/Button';
import { remote } from 'electron';
import JSONStream from 'JSONStream';
import routes from '../constants/routes';
import Registry from '../plugins/Registry';

const { dialog } = remote;

type Props = {
  match: any
};

Modal.setAppElement('body');

class ManagePlugin extends Component<Props> {
  props: Props;

  constructor(props) {
    super(props);

    this.registry = new Registry();

    const { pluginId } = props.match.params;

    let state = {};
    if (pluginId && pluginId !== ':pluginId') {
      const plugin = this.registry.findPlugin(pluginId);
      this.pageTitle = `Edit plugin ${plugin.id}`;
      state = {
        id: plugin.id,
        name: plugin.name,
        version: plugin.version,
        location: plugin.location,
        enabled: plugin.enabled
      };
    } else {
      this.pageTitle = 'Add plugin';
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.onFieldChange = this.onFieldChange.bind(this);
    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);

    this.state = {
      modalIsOpen: false,
      modalTitle: '',
      modalMessage: '',
      ...state
    };
  }

  handleSubmit(evt) {
    console.log(this.state);

    evt.preventDefault();

    const { id, name, version, location, enabled } = this.state;

    this.registry.createPlugin(
      id,
      name || id,
      version,
      location,
      enabled || true
    );

    // eslint-disable-next-line react/prop-types
    const { history } = this.props;
    history.push(routes.SERVERS);
    return true;
  }

  onFieldChange(property, evt) {
    const state = {};

    if (evt.target.type === 'checkbox') {
      state[property] = evt.target.checked;
    } else {
      state[property] = evt.target.value;
    }

    this.setState(state);
  }

  openModal(modalTitle, modalMessage) {
    this.setState({ modalIsOpen: true, modalTitle, modalMessage });
  }

  afterOpenModal() {
    const { modalTitle } = this.state;
    console.log(`Opened modal with title ${modalTitle}`);
  }

  closeModal() {
    this.setState({ modalIsOpen: false, modalTitle: '', modalMessage: '' });
  }

  choosePlugin() {
    dialog.showOpenDialog(
      {
        properties: ['openFile', 'openDirectory']
      },
      files => {
        if (files === undefined || files.length === 0) {
          return '';
        }

        // TODO: Handle multiple paths.
        const pluginPath = path.resolve(files[0]);
        if (!fs.existsSync(pluginPath)) {
          // TODO: This should pop up a warning or modal or something.
          return '';
        }

        // Look for zip, package.json, or folder containing package.json, per:
        // https://docs.npmjs.com/about-packages-and-modules
        const pluginStats = fs.statSync(pluginPath);
        const isPackageArchive =
          pluginStats.isFile() && /^.*\.zip$/.test(path.basename(pluginPath));
        const isPackageDef =
          !isPackageArchive &&
          pluginStats.isFile() &&
          /^package\.json$/.test(path.basename(pluginPath));
        const isPackageFolder =
          !isPackageDef &&
          pluginStats.isDirectory() &&
          fs.existsSync(path.resolve(pluginPath, 'package.json'));

        if (isPackageArchive) {
          fs.createReadStream(pluginPath)
            .pipe(unzipper.ParseOne(/^package\.json$/))
            // .pipe(JSONStream.parse(['*', true, { emitKey: true }]))
            .pipe(JSONStream.parse('*'))
            .on('data', data => {
              console.log(data);
            })
            .on('entry', entry => {
              const properties = JSON.parse(entry);
              this.setState({
                id: properties.name,
                name: properties.productName || properties.name,
                version: properties.version,
                location: pluginPath,
                enabled: true
              });
              const { id, version } = this.state;
              console.log(
                `Parsed by JSON.parse() from ${pluginPath}: ID ${id}, version ${version}`
              );
            });
          fs.createReadStream(pluginPath)
            .pipe(unzipper.ParseOne(/^package\.json$/))
            .pipe(JSONStream.parse(['*', { emitKey: true }]))
            .on('data', data => {
              console.log(data);
            });
          // fs.createReadStream(pluginPath)
          //   .pipe(unzipper.ParseOne('package.json'))
          //   .pipe(JSONStream.parse(['*', { emitKey: true }]))
          //   .on('data', data => {
          //     console.log(`${data.key}: ${data.value}`);
          //   })
          //   .on('error', error => {
          //     console.error(error);
          //   });
        } else if (isPackageDef || isPackageFolder) {
          // TODO: Handle a plugin folder or package definition.
          const packagePath = isPackageDef
            ? pluginPath
            : path.resolve(pluginPath, 'package.json');
          const modulePath = isPackageFolder
            ? pluginPath
            : path.dirname(pluginPath);
          fs.readFile(packagePath, (error, data) => {
            if (error) {
              throw error;
            }
            const properties = JSON.parse(data);
            console.log(`Module path: ${modulePath}`);
            console.log(`Main: ${properties.main}`);
            console.log(`Full path: ${path.resolve(modulePath, properties.main)}`);
            const componentPath = path.resolve(modulePath, properties.main).replace(/\.[^/.]+$/, "");
            console.log(`Component path: ${componentPath}`);
            this.setState({
              id: properties.name,
              name: properties.productName || properties.name,
              version: properties.version,
              location: componentPath,
              enabled: true
            });
            const { id, version, location } = this.state;
            console.log(
              `Parsed by JSON.parse() from ${packagePath}: ID ${id}, version ${version}, location ${location}`
            );
          });
          // TODO: Get the streaming parsing working.
          // console.log(`Parsed by JSONStream.parse():`);
          // fs.createReadStream(packagePath, { encoding: 'utf8' })
          //   .pipe(JSONStream.parse(['*', { emitKey: true }]))
          //   .on('data', data => {
          //     console.log(` * ${data.key}: ${data.value}`);
          //   });
          // .on('data', data => {
          //   console.log(data);
          // })
          // .on('name', data => {
          //   console.log(data);
          // })
          // .on('version', data => {
          //   console.log(data);
          // })
          // .on('error', error => {
          //   console.error(error);
          // });
          // fs.readFile(packagePath, data => {
          //   const packageProperties = JSON.parse(data);
          //   this.state.id = packageProperties.id;
          //   this.state.version = packageProperties.version;
          // }).catch(error => console.error(error));
        }
      }
    );
  }

  render() {
    const {
      modalIsOpen,
      modalTitle,
      modalMessage,
      id,
      name,
      version,
      location,
      enabled
    } = this.state;
    return (
      <div id="managePlugin" className="container-fluid">
        <Modal
          isOpen={modalIsOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          contentLabel="pluginModal"
        >
          <h3>{modalTitle}</h3>
          <div>{modalMessage}</div>
          <button type="button" onClick={this.closeModal}>
            Close
          </button>
        </Modal>
        <div className="modal-content">
          <form action="" id="loginForm" onSubmit={this.handleSubmit}>
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLongTitle">
                {this.pageTitle}
              </h5>
            </div>
            <div className="modal-body">
              <div className="form-group">
                <label htmlFor="pluginId" className="col-form-label">
                  Plugin ID:
                </label>
                <input
                  type="text"
                  className="form-control"
                  name="pluginId"
                  id="pluginId"
                  placeholder="pluginId"
                  defaultValue={id}
                  required
                  onChange={evt => {
                    this.onFieldChange('id', evt);
                  }}
                />
              </div>
              <div className="form-group">
                <label htmlFor="pluginName" className="col-form-label">
                  Plugin Name:
                </label>
                <input
                  type="text"
                  className="form-control"
                  name="pluginName"
                  id="pluginName"
                  placeholder="pluginName"
                  defaultValue={name}
                  required
                  onChange={evt => {
                    this.onFieldChange('name', evt);
                  }}
                />
              </div>
              <div className="form-group">
                <label htmlFor="pluginVersion" className="col-form-label">
                  Version:
                </label>
                <input
                  type="text"
                  className="form-control"
                  name="pluginVersion"
                  id="pluginVersion"
                  defaultValue={version}
                  placeholder="Plugin version"
                  required
                  onChange={evt => {
                    this.onFieldChange('version', evt);
                  }}
                />
              </div>
              <div className="form-group">
                <label htmlFor="pluginLocation" className="col-form-label">
                  Location:
                </label>
                <input
                  type="text"
                  className="form-control"
                  name="pluginLocation"
                  id="pluginLocation"
                  defaultValue={location}
                  disabled
                />
                <Button
                  variant="outline-primary btn-bigfriendly"
                  block="true"
                  onClick={() => this.choosePlugin()}
                >
                  Choose...
                </Button>
              </div>
              <div className="form-check text-right">
                <input
                  className="form-check-input"
                  type="checkbox"
                  defaultChecked={enabled}
                  name="pluginEnabled"
                  id="pluginEnabled"
                  onChange={evt => {
                    this.onFieldChange('enabled', evt);
                  }}
                />
                <label className="form-check-label" htmlFor="pluginEnabled">
                  Enabled
                </label>
              </div>
            </div>
            <div className="modal-footer">
              <Link to={routes.SERVERS}>
                <button
                  type="button"
                  className="btn btn-gray"
                  data-dismiss="modal"
                >
                  Cancel
                </button>
              </Link>
              <button type="submit" className="btn btn-blue">
                Save
              </button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default withRouter(ManagePlugin);

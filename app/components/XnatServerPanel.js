// @flow
import React, { Component } from 'react';
import Modal from 'react-modal';
import { Link } from 'react-router-dom';
import OverlayTrigger from 'react-bootstrap/es/OverlayTrigger';
import Tooltip from 'react-bootstrap/es/Tooltip';
import Button from 'react-bootstrap/es/Button';
import ButtonGroup from 'react-bootstrap/es/ButtonGroup';
import ServerStore from '@xnat/xnat-js/src/auth/ServerStore';
import Server from '@xnat/xnat-js/src/auth/Server';
import routes from '../constants/routes';

Modal.setAppElement('body');

type Props = {
  server: Server,
  servers: ServerStore,
  openModal: (
    modalTitle: string,
    modalMessage: string,
    onOk: () => void,
    onCancel: () => void
  ) => void
};

export default class XnatServerPanel extends Component<Props> {
  props: Props;

  constructor(props) {
    super(props);

    this.server = props.server;
    this.servers = props.servers;
    this.openModal = props.openModal;

    this.promptToDelete = this.promptToDelete.bind(this);
  }

  promptToDelete() {
    console.log(`About to delete server ${this.server.key}`);
    this.openModal(
      'Are you sure?',
      `You're about to delete the ${this.server.address} entry for user ${
        this.server.username
      }. Click 'OK' to continue or 'Cancel' to return.`,
      () => {
        console.log(`Deleting server entry ${this.server.key}`);
        this.servers.deleteServer(this.server.key);
      }
    );
  }

  render() {
    const { server } = this.props;
    return (
      <div className="inventory-list-item">
        <OverlayTrigger
          key={`trigger-server-${server.key}`}
          placement="bottom"
          overlay={
            <Tooltip
              id={`tooltip-server-${server.key}`}
              className="user-name text-left"
            >
              <table className="text-left small">
                <tbody>
                  <tr>
                    <th>Server:</th>
                    <td>{server.address}</td>
                  </tr>
                  <tr>
                    <th>User:</th>
                    <td>{server.username}</td>
                  </tr>
                </tbody>
              </table>
            </Tooltip>
          }
        >
          <ButtonGroup id={`server-${server.key}`}>
            <Link to={routes.PLUGINS_PAGE}>
              <Button
                variant="outline-primary btn-bigfriendly"
                data-key={server.key}
                block="true"
              >
                <b>{server.alias}</b>
              </Button>
            </Link>
            <Link to={`/manageServer/${this.server.key}`}>
              <Button
                variant="outline-primary btn-bigfriendly"
                data-key={`edit-${server.key}`}
              >
                <img src="../resources/icons/file-edit-16.png" alt="Edit" />
              </Button>
            </Link>
            <Button
              variant="outline-primary btn-bigfriendly"
              data-key={`delete-${server.key}`}
              onClick={() => this.promptToDelete(server.key)}
            >
              <img src="../resources/icons/file-delete-16.png" alt="Delete" />
            </Button>
          </ButtonGroup>
        </OverlayTrigger>
      </div>
    );
  }
}

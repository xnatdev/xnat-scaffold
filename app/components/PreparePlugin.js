/* eslint-disable flowtype/no-weak-types */
// @flow
import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import Button from 'react-bootstrap/es/Button';
import ToggleButton from 'react-bootstrap/es/ToggleButton';
import ToggleButtonGroup from 'react-bootstrap/es/ToggleButtonGroup';
import ServerStore from '@xnat/xnat-js/src/auth/ServerStore';
import Server from '@xnat/xnat-js/src/auth/Server';
import routes from '../constants/routes';
import Registry from '../plugins/Registry';
import ScaffoldPlugin from '../plugins/ScaffoldPlugin';

type Props = {
  match: any
};

class PreparePlugin extends Component<Props> {
  props: Props;

  plugin: ScaffoldPlugin;

  servers: ServerStore;

  registry: Registry;

  selected: Server[];

  constructor(props) {
    super(props);

    const { pluginId } = props.match.params;

    this.servers = new ServerStore();
    this.registry = new Registry();
    this.selected = [];
    this.plugin = this.registry.findPlugin(pluginId);
    this.hasServers = this.servers.getServers().length > 0;
  }

  handleClick(key, event) {
    if (event.currentTarget.control.type === 'checkbox') {
      if (event.currentTarget.control.checked) {
        this.addServer(key);
      } else {
        this.removeServer(key);
      }
    }
    this.displayServers();
  }

  hasServer(key) {
    return this.locateServer(key) >= 0;
  }

  locateServer(key) {
    return this.selected.findIndex(server => key === server.id);
  }

  addServer(key) {
    if (this.hasServer(key)) {
      console.log(
        `Received request to add server ${key} to selected, but it's already there.`
      );
    } else {
      console.log(`Adding server ${key} to selected servers`);
      this.selected.push(this.servers.getServer(key));
    }
  }

  removeServer(key) {
    if (this.hasServer(key)) {
      console.log(`Removing server ${key} from selected servers`);
      this.selected.splice(this.locateServer(key), 1);
    } else {
      console.log(
        `Received request to remove server ${key} to selected, but it's not there.`
      );
    }
  }

  displayServers() {
    console.log(
      `Selected servers: ${this.selected.map(server => server.key).join(', ')}`
    );
  }

  render() {
    const configuredServers = this.hasServers ? (
      <ToggleButtonGroup type="checkbox">
        {this.servers.getServers().map(server => (
          <ToggleButton
            type="checkbox"
            id={server.key}
            key={server.key}
            value={server.key}
            onClick={this.handleClick.bind(this, server.key)}
          >
            <b>{server.alias || `${server.username}@${server.address}`}</b>
          </ToggleButton>
        ))}
      </ToggleButtonGroup>
    ) : (
      <ul>
        <li>No servers configured</li>
      </ul>
    );
    return (
      <div className="container" data-tid="container">
        <h3>Launching Plugin {this.plugin.display}</h3>
        <p>Select one or more server definitions to use with this plugin:</p>
        {configuredServers}
        <div>
          <Link
            to={{
              pathname: `/plugins/${this.plugin.id}/launch`,
              state: {
                servers: this.selected
              }
            }}
          >
            <Button
              variant="outline-primary btn-bigfriendly"
              data-plugin={this.plugin}
            >
              Launch plugin {this.plugin.display}
            </Button>
          </Link>
          <Link to={routes.SERVERS}>
            <Button variant="outline-primary btn-bigfriendly">Go back</Button>
          </Link>
        </div>
      </div>
    );
  }
}

export default withRouter(PreparePlugin);

import React, { Fragment } from 'react';
import { render } from 'react-dom';
import { AppContainer as ReactHotAppContainer } from 'react-hot-loader';
import ServerStore from '@xnat/xnat-js/src/auth/ServerStore';
import Root from './containers/Root';
import { configureStore, history } from './store/configureStore';
import './app.global.css';
import Registry from './plugins/Registry';

const store = configureStore();
const AppContainer = process.env.PLAIN_HMR ? Fragment : ReactHotAppContainer;
const plugins = new Registry();
const servers = new ServerStore();

render(
  <AppContainer>
    <Root store={store} history={history} plugins={plugins} servers={servers} />
  </AppContainer>,
  document.getElementById('root')
);

if (module.hot) {
  module.hot.accept('./containers/Root', () => {
    // eslint-disable-next-line global-require
    const NextRoot = require('./containers/Root').default;
    render(
      <AppContainer>
        <NextRoot
          store={store}
          history={history}
          plugins={plugins}
          servers={servers}
        />
      </AppContainer>,
      document.getElementById('root')
    );
  });
}

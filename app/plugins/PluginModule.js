let pluginModule;

//
// https://techsparx.com/nodejs/esnext/dynamic-import.html
//
async function model() {
  if (pluginModule) {
    return pluginModule;
  }
  pluginModule = await import(`${process.env.MODULE_MODEL}`);
  return pluginModule;
}

export async function whatIsIt() {
  return typeof (await model());
}

export async function check(key) {
  return (await model()).check(key);
}

export default class ScaffoldPlugin {
  id: string;

  name: string;

  version: string;

  location: string;

  enabled: boolean;

  display: string;

  constructor(id, name, version, location, enabled) {
    this.id = id;
    this.name = name;
    this.version = version;
    // TODO: Figure out why the display() method doesn't work.
    this.display = `${this.name} v${this.version}`;
    this.location = location.replace(/\/package\.json$/, '');
    this.enabled = enabled;
  }
}
